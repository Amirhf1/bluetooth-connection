package com.amirhf.avrblue;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {

    private static final boolean f1D = true;
    public static final String DEVICE_NAME = "device_name";
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_TOAST = 5;
    public static final int MESSAGE_WRITE = 3;
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private static final String TAG = "TAG";
    public static final String TOAST = "toast";

    private Button btnSeeting1, btnSeeting2, btnSeeting3,
            btnControl1, btnControl2, btnControl3, btnControl4, btnControl5, btnControl6,
            btnControl7, btnControl8, btnControl9, getBtnControl10, getBtnControl11, getBtnControl12,
            getBtnControl13, getBtnControl14, getBtnControl15, getBtnControl16, getBtnControl17,
            getBtnControl18, getBtnControl19, getBtnControl20, getBtnControl21, getBtnControl22,
            getBtnControl23, getBtnControl24, getBtnControl25, getBtnControl26, getBtnControl27,
            getBtnControl28, getBtnControl29, getBtnControl30, getBtnControl31, getBtnControl32,
            getBtnControl33, getBtnControl34, getBtnControl35, getBtnControl36, getBtnControl37,
            getBtnControl38, getBtnControl39, getBtnControl40, getBtnControl41;


//    private Button[] Button_control = new Button[this.Button_control_ids.length];
//    private final int[] Button_control_ids = new int[]{R.id.Button_control1, R.id.Button_control2,
//            R.id.Button_control3, R.id.Button_control4, R.id.Button_control5, R.id.Button_control6,
//            R.id.Button_control7, R.id.Button_control8, R.id.Button_control9};

//    private Button[] Button_setting = new Button[this.Button_setting_ids.length];
//    private final int[] Button_setting_ids = new int[]{R.id.Button_setting1, R.id.Button_setting2,
//            R.id.Button_setting3};


    //    View.OnClickListener button_click = new C00001();
    private ArrayAdapter<String> mAdapter;
    private BluetoothAdapter mBluetoothAdapter = null;
    private BluetoothService mChatService = null;
    private String mConnectedDeviceName = null;
    private Context mContext;
    private ListView mConversationView0;
    private final Handler mHandler = new C00012();
    private StringBuffer mOutStringBuffer;
    private TextView mTitle;


    class C00001 implements View.OnClickListener {
        C00001() {
        }

        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.Button_setting1:
                    MainActivity.this.startActivityForResult(new Intent(MainActivity.this.mContext, DeviceListActivity.class), 1);
                    return;
                case R.id.Button_setting2:
                    MainActivity.this.ensureDiscoverable();
                    return;
                case R.id.Button_setting3:
                    MainActivity.this.startActivity(new Intent(MainActivity.this.mContext, Prefer.class));
                    return;
                case R.id.Button_control1:
                    MainActivity.this.sendData(0);
                    return;
                case R.id.Button_control2:
                    MainActivity.this.sendData(1);
                    return;
                case R.id.Button_control3:
                    MainActivity.this.sendData(2);
                    return;
                case R.id.Button_control4:
                    MainActivity.this.sendData(3);
                    return;
                case R.id.Button_control5:
                    MainActivity.this.sendData(4);
                    return;
                case R.id.Button_control6:
                    MainActivity.this.sendData(5);
                    return;
                case R.id.Button_control7:
                    MainActivity.this.sendData(6);
                    return;
                case R.id.Button_control8:
                    MainActivity.this.sendData(7);
                    return;
                case R.id.Button_control9:
                    MainActivity.this.sendData(8);
                    return;
                // me
                case R.id.Button_control10:
                    MainActivity.this.sendData(9);
                    return;

                case R.id.Button_control11:
                    MainActivity.this.sendData(10);
                    return;

                case R.id.Button_control12:
                    MainActivity.this.sendData(11);
                    return;

                case R.id.Button_control13:
                    MainActivity.this.sendData(12);
                    return;

                case R.id.Button_control14:
                    MainActivity.this.sendData(13);
                    return;

                case R.id.Button_control15:
                    MainActivity.this.sendData(14);
                    return;

                case R.id.Button_control16:
                    MainActivity.this.sendData(15);
                    return;

                case R.id.Button_control17:
                    MainActivity.this.sendData(16);
                    return;

                case R.id.Button_control18:
                    MainActivity.this.sendData(17);
                    return;

                case R.id.Button_control19:
                    MainActivity.this.sendData(18);
                    return;

                case R.id.Button_control20:
                    MainActivity.this.sendData(19);
                    return;

                case R.id.Button_control21:
                    MainActivity.this.sendData(20);
                    return;

                case R.id.Button_control22:
                    MainActivity.this.sendData(21);
                    return;

                case R.id.Button_control23:
                    MainActivity.this.sendData(22);
                    return;

                case R.id.Button_control24:
                    MainActivity.this.sendData(23);
                    return;

                case R.id.Button_control25:
                    MainActivity.this.sendData(24);
                    return;

                case R.id.Button_control26:
                    MainActivity.this.sendData(25);
                    return;

                case R.id.Button_control27:
                    MainActivity.this.sendData(26);
                    return;

                case R.id.Button_control28:
                    MainActivity.this.sendData(27);
                    return;

                case R.id.Button_control29:
                    MainActivity.this.sendData(28);
                    return;

                case R.id.Button_control30:
                    MainActivity.this.sendData(29);
                    return;

                case R.id.Button_control31:
                    MainActivity.this.sendData(30);
                    return;

                case R.id.Button_control32:
                    MainActivity.this.sendData(31);
                    return;

                case R.id.Button_control33:
                    MainActivity.this.sendData(32);
                    return;

                case R.id.Button_control34:
                    MainActivity.this.sendData(33);
                    return;

                case R.id.Button_control35:
                    MainActivity.this.sendData(34);
                    return;

                case R.id.Button_control36:
                    MainActivity.this.sendData(35);
                    return;

                case R.id.Button_control37:
                    MainActivity.this.sendData(36);
                    return;

                case R.id.Button_control38:
                    MainActivity.this.sendData(37);
                    return;

                case R.id.Button_control39:
                    MainActivity.this.sendData(38);
                    return;

                case R.id.Button_control40:
                    MainActivity.this.sendData(39);
                    return;

                case R.id.Button_control41:
                    MainActivity.this.sendData(40);
                    return;

                default:
                    return;
            }
        }
    }

    class C00012 extends Handler {
        C00012() {
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    Log.i(MainActivity.TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                    switch (msg.arg1) {
                        case 0:
                        case 1:
                            MainActivity.this.mTitle.setText(R.string.title_not_connected);
                            return;
                        case 2:
                            MainActivity.this.mTitle.setText(R.string.title_connecting);
                            return;
                        case 3:
                            MainActivity.this.mTitle.setText(R.string.title_connected_to);
                            MainActivity.this.mTitle.append(MainActivity.this.mConnectedDeviceName);
                            MainActivity.this.mAdapter.clear();
                            return;
                        default:
                            return;
                    }
                case 2:
                    // MainActivity.this.mAdapter.add(new String((int[]) msg.obj, 0, msg.arg1));
                    MainActivity.this.mAdapter.add(new String((byte[]) msg.obj, 0, msg.arg1));
                    //MainActivity.this.mAdapter.add(new String((char[]) msg.obj, 0, msg.arg1));

                    // MainActivity.this.mAdapter.add(new String(msg.obj, 0, msg.arg1));
                    return;
                case 4:
                    MainActivity.this.mConnectedDeviceName = msg.getData().getString(MainActivity.DEVICE_NAME);
                    Toast.makeText(MainActivity.this.getApplicationContext(), new
                            StringBuilder(MainActivity.this
                            .getString(R.string.title_connected_to))
                            .append(MainActivity.this.mConnectedDeviceName)
                            .toString(), Toast.LENGTH_SHORT).show();
                    return;
                case MainActivity.MESSAGE_TOAST /*5*/:
                    Toast.makeText(MainActivity.this.getApplicationContext(), msg.getData().getString(MainActivity.TOAST), Toast.LENGTH_SHORT).show();
                    return;
                default:
                    return;
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i("TAG", "MainActivity");

        this.mContext = this;
        this.mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (this.mBluetoothAdapter == null) {
            Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        initWidget();


        btnSeeting1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.startActivityForResult(new Intent(MainActivity.this.mContext,
                        DeviceListActivity.class), 1);
            }
        });


        btnSeeting2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.ensureDiscoverable();

            }
        });


        btnSeeting3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.startActivity(new
                        Intent(MainActivity.this.mContext, Prefer.class));
            }
        });


        // Control
        btnControl1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(0);
            }
        });


        btnControl2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(1);
            }
        });

        btnControl3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(2);
            }
        });

        btnControl4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(3);
            }
        });

        btnControl5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(4);
            }
        });

        btnControl6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(5);
            }
        });

        btnControl7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(6);
            }
        });

        btnControl8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(7);
            }
        });

        btnControl9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(8);
            }
        });


        getBtnControl10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(9);
            }
        });

        getBtnControl11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(10);
            }
        });

        getBtnControl12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(11);
            }
        });

        getBtnControl13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(12);
            }
        });

        getBtnControl14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(13);
            }
        });

        getBtnControl15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(14);
            }
        });

        getBtnControl16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(15);
            }
        });

        getBtnControl17.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(16);
            }
        });

        getBtnControl18.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(17);
            }
        });

        getBtnControl19.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(18);
            }
        });

        getBtnControl20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(19);
            }
        });

        getBtnControl21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(20);
            }
        });

        getBtnControl22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(21);
            }
        });

        getBtnControl23.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(22);
            }
        });

        getBtnControl24.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(23);
            }
        });

        getBtnControl25.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(24);
            }
        });

        getBtnControl26.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(25);
            }
        });

        getBtnControl27.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(26);
            }
        });

        getBtnControl28.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(27);
            }
        });

        getBtnControl29.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(28);
            }
        });

        getBtnControl30.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(29);
            }
        });

        getBtnControl31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(30);
            }
        });

        getBtnControl32.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(31);
            }
        });

        getBtnControl33.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(32);
            }
        });

        getBtnControl34.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(33);
            }
        });

        getBtnControl35.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(34);
            }
        });

        getBtnControl36.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(35);
            }
        });

        getBtnControl37.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(36);
            }
        });

        getBtnControl38.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(37);
            }
        });

        getBtnControl39.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(38);
            }
        });

        getBtnControl40.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(39);
            }
        });

        getBtnControl41.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.sendData(40);
            }
        });

    }

    private void initWidget() {

        btnSeeting1 = findViewById(R.id.Button_setting1);
        btnSeeting2 = findViewById(R.id.Button_setting2);
        btnSeeting3 = findViewById(R.id.Button_setting3);

        btnControl1 = findViewById(R.id.Button_control1);
        btnControl2 = findViewById(R.id.Button_control2);
        btnControl3 = findViewById(R.id.Button_control3);
        btnControl4 = findViewById(R.id.Button_control4);
        btnControl5 = findViewById(R.id.Button_control5);
        btnControl6 = findViewById(R.id.Button_control6);
        btnControl7 = findViewById(R.id.Button_control7);
        btnControl8 = findViewById(R.id.Button_control8);
        btnControl9 = findViewById(R.id.Button_control9);

        getBtnControl10 = findViewById(R.id.Button_control10);
        getBtnControl11 = findViewById(R.id.Button_control11);
        getBtnControl12 = findViewById(R.id.Button_control12);
        getBtnControl13 = findViewById(R.id.Button_control13);
        getBtnControl14 = findViewById(R.id.Button_control14);
        getBtnControl15 = findViewById(R.id.Button_control15);
        getBtnControl16 = findViewById(R.id.Button_control16);
        getBtnControl17 = findViewById(R.id.Button_control17);
        getBtnControl18 = findViewById(R.id.Button_control18);
        getBtnControl19 = findViewById(R.id.Button_control19);
        getBtnControl20 = findViewById(R.id.Button_control20);
        getBtnControl21 = findViewById(R.id.Button_control21);
        getBtnControl22 = findViewById(R.id.Button_control22);
        getBtnControl23 = findViewById(R.id.Button_control23);
        getBtnControl24 = findViewById(R.id.Button_control24);
        getBtnControl25 = findViewById(R.id.Button_control25);
        getBtnControl26 = findViewById(R.id.Button_control26);
        getBtnControl27 = findViewById(R.id.Button_control27);
        getBtnControl28 = findViewById(R.id.Button_control28);
        getBtnControl29 = findViewById(R.id.Button_control29);
        getBtnControl30 = findViewById(R.id.Button_control30);
        getBtnControl31 = findViewById(R.id.Button_control31);
        getBtnControl32 = findViewById(R.id.Button_control32);
        getBtnControl33 = findViewById(R.id.Button_control33);
        getBtnControl34 = findViewById(R.id.Button_control34);
        getBtnControl35 = findViewById(R.id.Button_control35);
        getBtnControl36 = findViewById(R.id.Button_control36);
        getBtnControl37 = findViewById(R.id.Button_control37);
        getBtnControl38 = findViewById(R.id.Button_control38);
        getBtnControl39 = findViewById(R.id.Button_control39);
        getBtnControl40 = findViewById(R.id.Button_control40);
        getBtnControl41 = findViewById(R.id.Button_control41);
    }


    void init_buttons() {
//        int i;
//        for (i = 0; i < this.Button_control_ids.length; i++) {
//            this.Button_control[i] = findViewById(this.Button_control_ids[i]);
//            this.Button_control[i].setOnClickListener(this.button_click);
//        }
//        for (i = 0; i < this.Button_setting_ids.length; i++) {
//            this.Button_setting[i] = findViewById(this.Button_setting_ids[i]);
//            this.Button_setting[i].setOnClickListener(this.button_click);
//        }


    }

    private void sendData(int id) {
        sendMessage(Preference_function.getString(Preference_function.Keys_data[id], this.mContext));
    }

    public void onStart() {
        super.onStart();
        Log.e(TAG, "++ ON START ++");
        if (!this.mBluetoothAdapter.isEnabled()) {
            startActivityForResult(new Intent("android.bluetooth.adapter.action.REQUEST_ENABLE")
                    , 2);
        } else if (this.mChatService == null) {
            setupChat();
        }
    }

    public synchronized void onResume() {
        super.onResume();
        Log.e(TAG, "+ ON RESUME +");
        if (this.mChatService != null) {
            if (this.mChatService.getState() == 0) {
                this.mChatService.start();
            }

//            for (int i = 0; i < this.Button_control.length; i++) {
//                this.Button_control[i].setText(Preference_function.getString(Preference_function.Keys_name[i], this.mContext));
//            }

            // set name and data buttons.
            btnControl1.setText(Preference_function.getString(Preference_function.Keys_name[0], this.mContext));
            btnControl2.setText(Preference_function.getString(Preference_function.Keys_name[1], this.mContext));
            btnControl3.setText(Preference_function.getString(Preference_function.Keys_name[2], this.mContext));
            btnControl4.setText(Preference_function.getString(Preference_function.Keys_name[3], this.mContext));
            btnControl5.setText(Preference_function.getString(Preference_function.Keys_name[4], this.mContext));
            btnControl6.setText(Preference_function.getString(Preference_function.Keys_name[5], this.mContext));
            btnControl7.setText(Preference_function.getString(Preference_function.Keys_name[6], this.mContext));
            btnControl8.setText(Preference_function.getString(Preference_function.Keys_name[7], this.mContext));
            btnControl9.setText(Preference_function.getString(Preference_function.Keys_name[8], this.mContext));

            getBtnControl10.setText(Preference_function.getString(Preference_function.Keys_name[9], this.mContext));
            getBtnControl11.setText(Preference_function.getString(Preference_function.Keys_name[10], this.mContext));
            getBtnControl12.setText(Preference_function.getString(Preference_function.Keys_name[11], this.mContext));
            getBtnControl13.setText(Preference_function.getString(Preference_function.Keys_name[12], this.mContext));
            getBtnControl14.setText(Preference_function.getString(Preference_function.Keys_name[13], this.mContext));
            getBtnControl15.setText(Preference_function.getString(Preference_function.Keys_name[14], this.mContext));
            getBtnControl16.setText(Preference_function.getString(Preference_function.Keys_name[15], this.mContext));
            getBtnControl17.setText(Preference_function.getString(Preference_function.Keys_name[16], this.mContext));
            getBtnControl18.setText(Preference_function.getString(Preference_function.Keys_name[17], this.mContext));
            getBtnControl19.setText(Preference_function.getString(Preference_function.Keys_name[18], this.mContext));
            getBtnControl20.setText(Preference_function.getString(Preference_function.Keys_name[19], this.mContext));
            getBtnControl21.setText(Preference_function.getString(Preference_function.Keys_name[20], this.mContext));
            getBtnControl22.setText(Preference_function.getString(Preference_function.Keys_name[21], this.mContext));
            getBtnControl23.setText(Preference_function.getString(Preference_function.Keys_name[22], this.mContext));
            getBtnControl24.setText(Preference_function.getString(Preference_function.Keys_name[23], this.mContext));
            getBtnControl25.setText(Preference_function.getString(Preference_function.Keys_name[24], this.mContext));
            getBtnControl26.setText(Preference_function.getString(Preference_function.Keys_name[25], this.mContext));
            getBtnControl27.setText(Preference_function.getString(Preference_function.Keys_name[26], this.mContext));
            getBtnControl28.setText(Preference_function.getString(Preference_function.Keys_name[27], this.mContext));
            getBtnControl29.setText(Preference_function.getString(Preference_function.Keys_name[28], this.mContext));
            getBtnControl30.setText(Preference_function.getString(Preference_function.Keys_name[29], this.mContext));
            getBtnControl31.setText(Preference_function.getString(Preference_function.Keys_name[30], this.mContext));
            getBtnControl32.setText(Preference_function.getString(Preference_function.Keys_name[31], this.mContext));
            getBtnControl33.setText(Preference_function.getString(Preference_function.Keys_name[32], this.mContext));
            getBtnControl34.setText(Preference_function.getString(Preference_function.Keys_name[33], this.mContext));
            getBtnControl35.setText(Preference_function.getString(Preference_function.Keys_name[34], this.mContext));
            getBtnControl36.setText(Preference_function.getString(Preference_function.Keys_name[35], this.mContext));
            getBtnControl37.setText(Preference_function.getString(Preference_function.Keys_name[36], this.mContext));
            getBtnControl38.setText(Preference_function.getString(Preference_function.Keys_name[37], this.mContext));
            getBtnControl39.setText(Preference_function.getString(Preference_function.Keys_name[38], this.mContext));
            getBtnControl40.setText(Preference_function.getString(Preference_function.Keys_name[39], this.mContext));
            getBtnControl41.setText(Preference_function.getString(Preference_function.Keys_name[40], this.mContext));
        }
    }


    private void setupChat() {
        Log.d(TAG, "setupChat()");
        this.mConversationView0 = findViewById(R.id.text_read);
        this.mAdapter = new ArrayAdapter(this, R.layout.message);
        this.mConversationView0.setAdapter(this.mAdapter);
        this.mTitle = findViewById(R.id.title);
        this.mChatService = new BluetoothService(this, this.mHandler);
        this.mOutStringBuffer = new StringBuffer();
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.mChatService != null) {
            this.mChatService.stop();
        }
        Log.e(TAG, "--- ON DESTROY ---");
    }

    private void ensureDiscoverable() {
        Log.d(TAG, "ensure discoverable");
        if (this.mBluetoothAdapter.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent("android.bluetooth.adapter.action.REQUEST_DISCOVERABLE");
            discoverableIntent.putExtra("android.bluetooth.adapter.extra.DISCOVERABLE_DURATION", 30);
            startActivity(discoverableIntent);
        }
    }

    private void sendMessage(String message) {
        if (this.mChatService.getState() != 3) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
        } else if (message.length() > 0) {
            this.mChatService.write(message.getBytes());
            this.mOutStringBuffer.setLength(0);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult " + resultCode);
        switch (requestCode) {
            case 1:
                if (resultCode == -1) {
                    this.mChatService.connect(this.mBluetoothAdapter.getRemoteDevice(data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS)));
                    return;
                }
                return;
            case 2:
                if (resultCode == -1) {
                    setupChat();
                    return;
                }
                Log.d(TAG, "BT not enabled");
                Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                finish();
                return;
            default:
                return;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_main_about:

                AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
                builder1.setMessage("Amir Hossin Fallah" + "\n" + "Android Dev."
                        + "\n" + "Telegram: @amirhf1");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "Call",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intent = new Intent(Intent.ACTION_DIAL);
                                intent.setData(Uri.parse("tel:+989199861260"));
                                startActivity(intent);
                            }
                        });

                builder1.setNegativeButton(
                        "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();


                return true;
            case R.id.menu_main_exit:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
