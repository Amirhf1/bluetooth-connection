package com.amirhf.avrblue;

import android.content.Context;
import android.content.SharedPreferences.Editor;

public class Preference_function {

    static final String[] Keys_data = new String[]{"data1", "data2", "data3", "data4", "data5",
            "data6", "data7", "data8", "data9",
            "data10", "data11", "data12", "data13", "data14", "data15", "data16", "data17", "data18",
            "data19", "data20", "data21", "data22", "data23", "data24", "data25", "data26", "data27",
            "data28", "data29", "data30", "data31", "data32", "data33", "data34", "data35", "data36",
            "data37", "data38", "data39", "data40", "data41"
    };

    public static final String[] Keys_name = new String[]{"name1", "name2", "name3", "name4",
            "name5", "name6", "name7", "name8", "name9", "name10",
            "name11", "name12", "name13", "name14", "name15", "name16", "name17", "name18", "name19",
            "name20", "name21", "name22", "name23", "name24", "name25", "name26", "name27", "name28",
            "name29", "name30", "name31", "name32", "name33", "name34", "name35", "name36", "name37",
            "name38", "name39", "name40", "name41"
    };

    public static final String Prefname = "BluetoothControl";

    public static void putString(String Key, String value, Context _context) {
        Editor editor = _context.getSharedPreferences(Prefname, 0).edit();
        editor.putString(Key, value);
        editor.commit();
    }

    public static void putNum(String Key, int value, Context _context) {
        Editor editor = _context.getSharedPreferences(Prefname, 0).edit();
        editor.putInt(Key, value);
        editor.commit();
    }

    public static String getString(String Key, Context _context) {
        return _context.getSharedPreferences(Prefname, 0).getString(Key, "");
    }

    public static int getNum(String Key, Context _context) {
        return _context.getSharedPreferences(Prefname, 0).getInt(Key, 0);
    }
}
