package com.amirhf.avrblue;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Prefer extends AppCompatActivity {

    private View.OnClickListener click = new click();

    private final int[] edit_data_ids = new int[]{R.id.edit_data1, R.id.edit_data2,
            R.id.edit_data3, R.id.edit_data4, R.id.edit_data5, R.id.edit_data6,
            R.id.edit_data7, R.id.edit_data8, R.id.edit_data9, R.id.edit_data10, R.id.edit_data11,
            R.id.edit_data12, R.id.edit_data13, R.id.edit_data14, R.id.edit_data15, R.id.edit_data16,
            R.id.edit_data17, R.id.edit_data18, R.id.edit_data19, R.id.edit_data20, R.id.edit_data21,
            R.id.edit_data22, R.id.edit_data23, R.id.edit_data24, R.id.edit_data25, R.id.edit_data26,
            R.id.edit_data27, R.id.edit_data28, R.id.edit_data29, R.id.edit_data30, R.id.edit_data31,
            R.id.edit_data32, R.id.edit_data33, R.id.edit_data34, R.id.edit_data35, R.id.edit_data36,
            R.id.edit_data37, R.id.edit_data38, R.id.edit_data39, R.id.edit_data40, R.id.edit_data41,
    };

    public final int[] edit_name_ids = new int[]{R.id.edit_name1, R.id.edit_name2,
            R.id.edit_name3, R.id.edit_name4, R.id.edit_name5, R.id.edit_name6,
            R.id.edit_name7, R.id.edit_name8, R.id.edit_name9, R.id.edit_name10, R.id.edit_name11,
            R.id.edit_name12, R.id.edit_name13, R.id.edit_name14, R.id.edit_name15, R.id.edit_name16,
            R.id.edit_name17, R.id.edit_name18, R.id.edit_name19, R.id.edit_name20, R.id.edit_name21,
            R.id.edit_name22, R.id.edit_name23, R.id.edit_name24, R.id.edit_name25, R.id.edit_name26,
            R.id.edit_name27, R.id.edit_name28, R.id.edit_name29, R.id.edit_name30, R.id.edit_name31,
            R.id.edit_name32, R.id.edit_name33, R.id.edit_name34, R.id.edit_name35, R.id.edit_name36,
            R.id.edit_name37, R.id.edit_name38, R.id.edit_name39, R.id.edit_name40, R.id.edit_name41,
    };

    private Context mContext;


    class click implements View.OnClickListener {
        click() {
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.Save:
                    int i;
                    for (i = 0; i < Prefer.this.edit_name_ids.length; i++) {
                        Preference_function.putString(Preference_function.Keys_name[i], ((EditText)
                                Prefer.this.findViewById(Prefer.this.edit_name_ids[i]))
                                .getText().toString(), Prefer.this.mContext);
                    }
                    for (i = 0; i < Prefer.this.edit_data_ids.length; i++) {
                        Preference_function.putString(Preference_function.Keys_data[i], ((EditText)
                                Prefer.this.findViewById(Prefer.this.edit_data_ids[i]))
                                .getText().toString(), Prefer.this.mContext);
                    }
                    Prefer.this.finish();
                    return;
                case R.id.Cancel:
                    Prefer.this.finish();
                    return;
                default:
                    return;
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prefer);
        this.mContext = this;

        Button Button_cancel = findViewById(R.id.Cancel);
        findViewById(R.id.Save).setOnClickListener(this.click);
        Button_cancel.setOnClickListener(this.click);
        init_Edits();


    }

    private void init_Edits() {

        int i;
        for (i = 0; i < this.edit_name_ids.length; i++) {
            ((EditText) findViewById(this.edit_name_ids[i])).setText(Preference_function
                    .getString(Preference_function.Keys_name[i], this.mContext));
        }
        for (i = 0; i < this.edit_data_ids.length; i++) {
            ((EditText) findViewById(this.edit_data_ids[i])).setText(Preference_function.getString(Preference_function.Keys_data[i], this.mContext));
        }


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
